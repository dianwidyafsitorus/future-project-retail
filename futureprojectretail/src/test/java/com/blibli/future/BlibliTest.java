package com.blibli.future;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


import java.util.concurrent.TimeUnit;


/**
 * created by Dian Widya Febrianti sitorus
 */
public class BlibliTest {
   WebDriver webDriver;
    static ExtentTest test;
    static ExtentReports report;

    @BeforeClass
    public static void startTest(){
        report = new ExtentReports(System.getProperty("user.dir")+"/src/BlibliReport/BlibliRetailReport.html");
    }


    @Before
    public void setupClass() throws Exception {
        BlibligetProperties blibligetProperties = new BlibligetProperties();
        String Browser = blibligetProperties.getBlibliProperty("properties.browser");
        if (Browser.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            webDriver = new ChromeDriver();
        } else if (Browser.equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            webDriver = new FirefoxDriver();
        } else {
            throw new Exception("Browser is not correct");
        }
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }


    @After
    public void tearDown() {
        webDriver.close();
    }

    @AfterClass
    public static void endTest() {
        report.endTest(test);
        report.flush();
    }

    @Test
    public void Runner() throws InterruptedException {
    BlibliRegister();
    ValidBlibliLogin();
    invalidUsernameLogin();
    invalidPasswordLogin();
    invalidBlibliLogin();
    CariBarang();
    KategoriGaleriIndonesia();
    checkBagNoLogin();
    jumlahBarangKosong();
    BlibliRetailTambahBag();
    CheckBag();
    BlibliRetailNoLogin();
    BeliSekarang();
    tambahkeWishlist();
    LihatWishlist();
    clickHapusBarang();
    clickBelanjaLagi();
    clickMetodePengiriman();
    BlibliRetailgulaku();
    lihatPromo();
}


        @Test
        public void BlibliRegister() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            BlibliProperties blibliProperties = new BlibliProperties(webDriver);
            String randomUsername = BlibliHomePage.createRandomUsername();
            test = report.startTest("test Blibli Register");
            blibliHomePage.openBlibli(webDriver);
            checkTitle();
            blibliHomePage.clickBtnDaftar();
            checkButtonDaftar();
            Thread.sleep(6000);
            blibliHomePage.inputRegister(randomUsername + "@gmail.com", blibliProperties.passwordKey);
            checkinputRegister();
            blibliHomePage.clickBtnSubmitDaftar();
        }

        @Test
        public void ValidBlibliLogin() throws InterruptedException {
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        BlibliProperties blibliProperties = new BlibliProperties(webDriver);
            test = report.startTest("test Blibli Valid Login");
           blibliHomePage.openBlibli(webDriver);
           checkTitle();
            blibliHomePage.clickBtnMasuk();
           checkButtonMasuk();
            blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordKey);
           checkinputRegister();
            blibliHomePage.clickBtnSubmit();
            checkverificationpage();
            blibliHomePage.beforelogout();
            blibliHomePage.logout();
            checkButtonLogout();
        }

      @Test
        public void invalidUsernameLogin() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            BlibliUtility blibliUtility = new BlibliUtility(webDriver);
            BlibliProperties blibliProperties = new BlibliProperties(webDriver);
            test = report.startTest("test Blibli inValid username Login");
          blibliHomePage.openBlibli(webDriver);
          checkTitle();
            blibliHomePage.clickBtnMasuk();
          checkButtonMasuk();
            blibliHomePage.inputMasuk(blibliProperties.usernameLogininvalid,blibliProperties.passwordKey);
          checkinputLogin();
            blibliHomePage.clickBtnSubmit();
          checkErrorMessage(webDriver,"Format alamat email tidak valid.");
        }

        @Test
        public void invalidPasswordLogin() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            BlibliUtility blibliUtility = new BlibliUtility(webDriver);
            BlibliProperties blibliProperties = new BlibliProperties(webDriver);
            test = report.startTest("test Blibli inValid password Login");
           blibliHomePage.openBlibli(webDriver);
            checkTitle();
            blibliHomePage.clickBtnMasuk();
            checkButtonMasuk();
            blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordinvalid);
            checkinputLogin();
            blibliHomePage.clickBtnSubmit();
            checkErrorMessage(webDriver,"Maaf, email atau kata sandi yang kamu masukkan salah.");
        }

       @Test
        public void invalidBlibliLogin() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            BlibliUtility blibliUtility = new BlibliUtility(webDriver);
            BlibliProperties blibliProperties = new BlibliProperties(webDriver);
            test = report.startTest("test Blibli inValid username dan password Login");
           blibliHomePage.openBlibli(webDriver);
            checkTitle();
            blibliHomePage.clickBtnMasuk();
            checkButtonMasuk();
            blibliHomePage.inputMasuk(blibliProperties.usernameLogininvalid,blibliProperties.passwordinvalid);
            checkinputLogin();
            blibliHomePage.clickBtnSubmit();
           checkErrorMessage(webDriver,"Format alamat email tidak valid.");
        }

        @Test
        public void CariBarang() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            BlibliUtility blibliUtility = new BlibliUtility(webDriver);
            BlibliProperties blibliProperties = new BlibliProperties(webDriver);
            BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
            BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
            test = report.startTest("test Blibli cari Item");
            blibliHomePage.openBlibli(webDriver);
            checkTitle();
            blibliHomePage.inputSearch(blibliProperties.barang1Key);
            checkbarang1();
            blibliSearchPage.clearTextfieldSearch();
            checkcleartextfield();
            blibliHomePage.inputSearch(blibliProperties.barang3Key);
            checkbarang3();
            Thread.sleep(6000);
            blibliHomePage.closeIklan();
            checkcloseiklan();
            blibliSearchPage.clearTextfieldSearch();
            checkcleartextfield();
            blibliHomePage.inputSearch(blibliProperties.barang4Key);
            checkbarang4();
            blibliSearchPage.clearTextfieldSearch();
            checkcleartextfield();
            blibliHomePage.inputSearch(blibliProperties.barang5Key);
            checkbarang5();
        }

        @Test
        public void BlibliRetailgulaku() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            BlibliProperties blibliProperties = new BlibliProperties(webDriver);
          BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
          BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
          BlibliPengiriman blibliPengiriman = new BlibliPengiriman(webDriver);
          BlibliBagPage blibliBagPage = new BlibliBagPage(webDriver);
          BlibliPembayaran blibliPembayaran = new BlibliPembayaran(webDriver);
            test = report.startTest("test Blibli retail Beli Sekarang gulaku");
          blibliHomePage.openBlibli(webDriver);
          checkTitle();
            blibliHomePage.clickBtnMasuk();
          checkButtonMasuk();
            blibliHomePage.inputMasuk(blibliProperties.usernameloginalamat,blibliProperties.passwordKey);
          checkinputLogin();
            blibliHomePage.clickBtnSubmit();
         checkverificationpage();
            blibliHomePage.inputSearch(blibliProperties.barang2Key);
            checkbarang2();
            blibliSearchPage.clickItemgulaku();
            checkClickgulaku();
            Thread.sleep(6000);
            blibliHomePage.closeIklan();
            checkcloseiklan();
            blibliDescriptionPage.clickTambahKuantitas();
            checkClickTambahKuantitas();
            blibliDescriptionPage.clickKurangKuantitas();
            checkClickKurangKuantitas();
            blibliDescriptionPage.clickBtnBeliSekarang();
            checkClickBtnBeliSekarang();
            blibliBagPage.clickBtnCheckOut();
            checkClickBtnCheckout();
            blibliPengiriman.clickBtnLanjut();
            checkClickBtnLanjut();
            blibliPengiriman.clickTambahAlamat();
            checkClickTambahAlamat();
            blibliPengiriman.clearTextfieldNamaPengirim();
            checkclearNamaPenerima();
            blibliPengiriman.inputNamapenerima(blibliProperties.namaPenerima);
            checkNamaPenerima();
            blibliPengiriman.inputNoTelepon(blibliProperties.noTelepon);
            checkNomorTelepon();
            blibliPengiriman.inputAlamatPengiriman(blibliProperties.alamatKey);
            checkAlamat();
            blibliPengiriman.clickpilihProvinsi();
            checkProvinsi();
            blibliPengiriman.clickpilihKota();
            checkKota();
            blibliPengiriman.clickpilihKecamatan();
            checkKecamatan();
            blibliPengiriman.clickpilihKelurahan();
            checkKelurahan();
            blibliPengiriman.clickBtnSimpan();
            checkClickBtnSimpan();
          blibliPengiriman.clickBtnLanjut();
          checkClickBtnLanjut();
          blibliPembayaran.clickBtnBayarSekarang();
          checkBtnBayarSekarang();
          checkThankYouPage();
            blibliHomePage.beforelogout();
          blibliHomePage.logout();
          checkButtonLogout();
        }

        @Test
        public void BlibliRetailTambahBag() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            BlibliProperties blibliProperties = new BlibliProperties(webDriver);
            BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
            BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
            test = report.startTest("test Blibli retail Tambah Bag");
            blibliHomePage.openBlibli(webDriver);
            checkTitle();
            blibliHomePage.clickBtnMasuk();
            checkButtonMasuk();
            blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordKey);
            checkinputRegister();
            blibliHomePage.clickBtnSubmit();
            checkverificationpage();
            blibliHomePage.inputSearch(blibliProperties.barang5Key);
            checkbarang5();
            blibliSearchPage.checklistwarna();
            checkClickPilihWarna();
            blibliSearchPage.clickItemSepatu2();
            checkClicksepatu();
            blibliDescriptionPage.clickBtnTambahKeBag();
            checkClickBtnTambahKeBag();
            blibliDescriptionPage.clickBag();
            checkClickItemBag();
            blibliHomePage.beforelogout();
            blibliHomePage.logout();
            checkButtonLogout();
        }

        @Test
        public void BlibliRetailNoLogin() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            BlibliProperties blibliProperties = new BlibliProperties(webDriver);
            BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
            BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
            test = report.startTest("test Blibli retail belum login");
           blibliHomePage.openBlibli(webDriver);
            checkTitle();
            blibliHomePage.inputSearch(blibliProperties.barang5Key);
            checkbarang5();
            blibliSearchPage.clickItemSepatu2();
            checkClicksepatu();
            blibliHomePage.closeIklan();
            blibliDescriptionPage.clickBtnBeliSekarang();
            checkClickBtnBeliSekarang();
            blibliHomePage.closeIklan();
            checkcloseiklan();
            blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordKey);
            checkinputRegister();
            blibliHomePage.clickBtnSubmit();
            checkverificationpage();
            blibliDescriptionPage.clickBag();
            checkClickItemBag();
            blibliHomePage.beforelogout();
            blibliHomePage.logout();
            checkButtonLogout();
        }

        @Test
        public void CheckBag() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            BlibliProperties blibliProperties = new BlibliProperties(webDriver);
            BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
            test = report.startTest("test Blibli retail check Bag");
            webDriver.get("https://www.blibli.com");
            checkTitle();
            blibliHomePage.clickBtnMasuk();
            checkButtonMasuk();
            blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordKey);
            checkinputRegister();
            blibliHomePage.clickBtnSubmit();
            checkverificationpage();
            blibliDescriptionPage.clickBag();
            checkClickItemBag();
            blibliHomePage.beforelogout();
            blibliHomePage.logout();
            checkButtonLogout();
        }

        @Test
        public void BeliSekarang() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            BlibliProperties blibliProperties = new BlibliProperties(webDriver);
            BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
            BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
            BlibliPengiriman blibliPengiriman = new BlibliPengiriman(webDriver);
            BlibliBagPage blibliBagPage = new BlibliBagPage(webDriver);
            BlibliPembayaran blibliPembayaran = new BlibliPembayaran(webDriver);
            test = report.startTest("test Blibli retail beli sekarang");
            blibliHomePage.openBlibli(webDriver);
            checkTitle();
            blibliHomePage.clickBtnMasuk();
            checkButtonMasuk();
            blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordKey);
            checkinputRegister();
            blibliHomePage.clickBtnSubmit();
            checkverificationpage();
            blibliHomePage.inputSearch(blibliProperties.barang5Key);
            checkbarang5();
            blibliSearchPage.clickItemSepatu2();
            checkClicksepatu();
            blibliDescriptionPage.clickBtnBeliSekarang();
            checkClickBtnBeliSekarang();
            blibliBagPage.clickBtnCheckOut();
            checkClickBtnCheckout();
            blibliPengiriman.clickBtnLanjut();
            checkClickBtnLanjut();
            blibliPembayaran.clickBtnBayarSekarang();
            checkBtnBayarSekarang();
            checkThankYouPage();
            Thread.sleep(3000);
            blibliHomePage.beforelogout();
            blibliHomePage.logout();
            checkButtonLogout();
        }

        @Test
        public void jumlahBarangKosong() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            BlibliProperties blibliProperties = new BlibliProperties(webDriver);
            BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
            BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
            test = report.startTest("test Blibli jumlah barang Kosong");
            blibliHomePage.openBlibli(webDriver);
            checkTitle();
            checkcloseiklan();
            blibliHomePage.clickBtnMasuk();
            checkButtonMasuk();
            blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordKey);
            checkinputRegister();
            blibliHomePage.clickBtnSubmit();
            checkverificationpage();
            blibliHomePage.inputSearch(blibliProperties.barang2Key);
            checkbarang2();
            blibliSearchPage.clickItemgulaku();
            checkClickgulaku();
            blibliDescriptionPage.jumlahbarang0();
            blibliDescriptionPage.clickBtnBeliSekarang();
            checkClickBtnBeliSekarang();
            blibliHomePage.beforelogout();
            blibliHomePage.logout();
            checkButtonLogout();
        }

        @Test
        public void KategoriGaleriIndonesia() throws InterruptedException {
            BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
            test = report.startTest("test Blibli kategori galeri indonesia");
            blibliHomePage.openBlibli(webDriver);
            checkTitle();
            blibliHomePage.clickbtnKategori();
            checkButtonKategori();
            blibliHomePage.clickGaleriIndonesia();
            checkGaleriIndonesia();
        }

    @Test
    public void checkBagNoLogin() throws InterruptedException {
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        BlibliProperties blibliProperties = new BlibliProperties(webDriver);
        BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
        test = report.startTest("test Blibli retail check Bag Tanpa Login");
        webDriver.get("https://www.blibli.com");
        checkTitle();
        blibliDescriptionPage.clickBag();
        Thread.sleep(6000);
        checkClickItemBag();
    }

       @Test
    public void tambahkeWishlist() throws InterruptedException {
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        BlibliProperties blibliProperties = new BlibliProperties(webDriver);
        BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
        BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
        test = report.startTest("test Blibli jumlah barang 0");
        blibliHomePage.openBlibli(webDriver);
        checkTitle();
        blibliHomePage.clickBtnMasuk();
        checkButtonMasuk();
        blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordKey);
        checkinputRegister();
        blibliHomePage.clickBtnSubmit();
        checkverificationpage();
        blibliHomePage.inputSearch(blibliProperties.barang1Key);
        checkbarang1();
        blibliSearchPage.clickItemTas();
        checkClickTas();
        blibliDescriptionPage.TambahWishlist();
        checkWishlist();
           blibliHomePage.beforelogout();
           blibliHomePage.logout();
           checkButtonLogout();
    }

    @Test
    public void LihatWishlist() throws InterruptedException {
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        BlibliProperties blibliProperties = new BlibliProperties(webDriver);
        BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
        BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
        test = report.startTest("test Blibli melihat jumlah barang di wishlist");
        blibliHomePage.openBlibli(webDriver);
        checkTitle();
        blibliHomePage.clickBtnMasuk();
        checkButtonMasuk();
        blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordKey);
        checkinputRegister();
        blibliHomePage.clickBtnSubmit();
        checkverificationpage();
        blibliHomePage.clickWishlist();
        checkWishlist();
        blibliHomePage.beforelogout();
        blibliHomePage.logout();
        checkButtonLogout();
    }


    @Test
    public void lihatPromo() throws InterruptedException {
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        BlibliProperties blibliProperties = new BlibliProperties(webDriver);
        BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
        BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
        test = report.startTest("test Blibli untuk melihat promo");
        blibliHomePage.openBlibli(webDriver);
        checkTitle();
        blibliHomePage.clickBtnMasuk();
        checkButtonMasuk();
        blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordKey);
        checkinputRegister();
        blibliHomePage.clickBtnSubmit();
        checkverificationpage();
        blibliHomePage.inputSearch(blibliProperties.barang1Key);
        checkbarang1();
        blibliSearchPage.clickItemTas();
        checkClickTas();
        blibliDescriptionPage.LihatPromo();
        checkLihatPromo();
        blibliDescriptionPage.LihatInfoPromo();
        checkLihatInfoPromo();
        blibliHomePage.beforelogout();
        blibliHomePage.logout();
        checkButtonLogout();
    }

    @Test
    public void clickHapusBarang() throws InterruptedException {
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        BlibliProperties blibliProperties = new BlibliProperties(webDriver);
        BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
        BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
        BlibliBagPage blibliBagPage = new BlibliBagPage(webDriver);
        test = report.startTest("test Blibli klik Hapus Barang");
        blibliHomePage.openBlibli(webDriver);
        checkTitle();
        blibliHomePage.clickBtnMasuk();
        checkButtonMasuk();
        blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordKey);
        checkinputRegister();
        blibliHomePage.clickBtnSubmit();
        checkverificationpage();
        blibliHomePage.inputSearch(blibliProperties.barang1Key);
        checkbarang1();
        blibliSearchPage.clickItemTas();
        checkbarang1();
        blibliDescriptionPage.clickBtnBeliSekarang();
        checkClickBtnBeliSekarang();
        blibliBagPage.clickHapusBarang();
        checkClickItemBag();
        blibliHomePage.beforelogout();
        blibliHomePage.logout();
        checkButtonLogout();
    }

    @Test
    public void clickBelanjaLagi() throws InterruptedException {
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        BlibliProperties blibliProperties = new BlibliProperties(webDriver);
        BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
        BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
        BlibliBagPage blibliBagPage = new BlibliBagPage(webDriver);
        test = report.startTest("test Blibli klik Belanja Lagi");
        blibliHomePage.openBlibli(webDriver);
        checkTitle();
        blibliHomePage.clickBtnMasuk();
        checkButtonMasuk();
        blibliHomePage.inputMasuk(blibliProperties.usernameLoginKey,blibliProperties.passwordKey);
        checkinputRegister();
        blibliHomePage.clickBtnSubmit();
        checkverificationpage();
        blibliHomePage.inputSearch(blibliProperties.barang1Key);
        checkbarang1();
        blibliSearchPage.clickItemTas();
        checkbarang1();
        blibliDescriptionPage.clickBtnBeliSekarang();
        checkClickBtnBeliSekarang();
        blibliBagPage.clickbelanjaLagi();
        checkTitle();
        blibliHomePage.beforelogout();
        blibliHomePage.logout();
        checkButtonLogout();
    }

    @Test
    public void clickMetodePengiriman() throws InterruptedException {
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        BlibliProperties blibliProperties = new BlibliProperties(webDriver);
        BlibliSearchPage blibliSearchPage = new BlibliSearchPage(webDriver);
        BlibliDescriptionPage blibliDescriptionPage = new BlibliDescriptionPage(webDriver);
        BlibliBagPage blibliBagPage = new BlibliBagPage(webDriver);
        test = report.startTest("test Blibli klik Metode pengiriman");
        blibliHomePage.openBlibli(webDriver);
        checkTitle();
        blibliHomePage.inputSearch(blibliProperties.barang2Key);
        checkbarang2();
        blibliSearchPage.clickmetodePengiriman();
        checkbarang2();
        blibliSearchPage.clickItemgulaku();
        checkbarang2();
    }




    //Method untuk melakukan pengecekan
    public void checkTitle() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
         String expectedTitle1 = webDriver.findElement(By.xpath("//a[@class='link'][contains(text(),'Jual di Blibli')]")).getText();
        if(expectedTitle1.equals("Jual di Blibli")){
            test.log(LogStatus.PASS, "Check page title", "Page title is " + expectedTitle1 +test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL, "Check page title", "Incorrect login page title " + webDriver.getTitle() +test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkLewati() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedTitle1 = webDriver.findElement(By.xpath("//a[@class='link'][contains(text(),'Jual di Blibli')]")).getText();
        if(expectedTitle1.equals("Jual di Blibli")){
            test.log(LogStatus.PASS,"Popup is passed","Popup closed"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL, "Popup is not passed", "PopUp not close " +test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkcloseiklan() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedTitle1 = webDriver.findElement(By.xpath("//a[@class='link'][contains(text(),'Jual di Blibli')]")).getText();
        if(expectedTitle1.equals("Jual di Blibli")){
            test.log(LogStatus.PASS,"iklan is passed", "iklan closed"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL, "iklan is not passed", "iklan not close " +test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkErrorMessage(WebDriver driver, String expectedMessage) throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String errorMessage = driver.findElement(By.className("error")).getText();
        if(errorMessage.equals(expectedMessage)) {
            test.log(LogStatus.PASS, "Check Error message", "Error message is " + expectedMessage +test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL, "Check page message", "View details below " +test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkButtonDaftar() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Daftar Akun Baru";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS, "Check Button Daftar", "Button Daftar is clicked show " + expectedPage +test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL, "Check Button Daftar", "Incorrect clicked button Daftar" +test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkinputRegister() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Daftar Akun Baru";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"username and password is filled","incorrect username and inpassword "+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"username and password is not filled","incorrect username and inpassword "+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkinputLogin() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Masuk ke akun kamu";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"username and password is filled","incorrect username or incorrect password "+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"username and password is not filled","correct username and password "+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkverificationpage() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Verifikasi nomor handphone";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"Login success open verification number page","success to opened verification number page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"Login fail to open verification number page","fail to opened verification number page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkButtonMasuk() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Masuk ke akun kamu";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS, "Check Button Masuk", "Button Masuk is clicked show " + expectedPage +test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL, "Check Button Masuk", "Incorrect clicked button Masuk"  +test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkButtonLogout() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedTitle1 = webDriver.findElement(By.xpath("//a[@class='link'][contains(text(),'Jual di Blibli')]")).getText();
        if(expectedTitle1.equals("Jual di Blibli")){
            test.log(LogStatus.PASS, "Check Button Logout", "Button Logout is clicked " +test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL, "Check Button Logout", "Incorrect clicked button Logout"  +test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }


    public void checkbarang1() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "tas wanta";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"taswanta search" , "taswanta result found"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"taswanta search" , "taswanta result not found"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkbarang2() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "gulaku";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"gulaku search", "gulaku result"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"gulaku search", "gulaku result"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkbarang3() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "samsungoppo";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"samsungoppo search", "samsungoppo result"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"samsungoppo search", "samsungoppo result not found"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkbarang4() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "tas1234567890";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"tas1234567890 search","tas1234567890 result"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"tas1234567890 search","tas1234567890 result not found"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkbarang5() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "sepatusport";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"sepatusport search", "sepatusport result"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"sepatusport search", "sepatusport result not found"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkcleartextfield() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = webDriver.findElement(By.xpath("//input[@placeholder='Kamu lagi cari apa?']")).getText();
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"text field is deleted", "textfield is empty"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"text field is not deleted", "textfield is not empty"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickgulaku() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Gulaku";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click item gulaku","item gulaku is clicked and open description page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click item gulaku","item gulaku is not clicked and not open description page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickTas() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Tas";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click item Tas","item Tas is clicked and open description page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click item Tas","item Tas is not clicked and not open description page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickTambahKuantitas() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "2";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click elemen tambah kuantitas","number of item is increase"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click elemen tambah kuantitas","number of item is not increase"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickKurangKuantitas() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "1";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"cari elemen kurang kuantitas","number of item is decrease"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click elemen kurang kuantitas","number of item is not deccrease"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickBtnBeliSekarang() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Ringkasan Pemesanan";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click button beli sekarang","button beli sekarang is clicked and open bag page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click button beli sekarang","button beli sekarang is not clicked and not open bag page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkWishlist() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Wishlist";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click wishlist","wishlist is clicked"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click wishlist","wishlist is not clicked"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkLihatPromo() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "promo";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click Button Lihat semua promo","Lihat semua promois clicked"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click Button Lihat semua promo","Lihat semua promo is not clicked"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkLihatInfoPromo() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Disc";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click Lihat Info promo","Lihat Info is clicked"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click Lihat Info promo","Lihat Info is not clicked"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickBtnTambahKeBag() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Sepatu";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click button tambah ke bag", "button tambah bag is clicked and show popup bag"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click button tambah ke bag", "button tambah bag is not clicked and not show popup bag"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickItemBag() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Bag";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click icon bag","icon bag is clicked and open bag page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click icon bag","icon bag is not clicked and not open bag page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickBtnCheckout() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Pengiriman";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"clik button checkout","button checkout is clicked and open pengiriman page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"clik button checkout","button checkout is not clicked and not open pengiriman page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickBtnLanjut() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Silakan lengkapi alamat pengiriman";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"clik button lanjut","button lanjut is clicked and show pop-up must fill alamat"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"clik button lanjut","button not lanjut is clicked and not show pop-up must fill alamat"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickLanjut() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Pembayaran";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click button lanjut","button lanjut is clicked and open pembayaran page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click button lanjut","button lanjut is not clicked and not open pembayaran page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickTambahAlamat() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Ubah Alamat Pengiriman";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.INFO,"click Tambah alamat","tambah alamat is clicked and show form alamat"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.INFO,"click Tambah alamat","tambah alamat is clicked and show form alamat"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkclearNamaPenerima() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = webDriver.findElement(By.xpath("//input[@id='gdn-receiptNm']")).getText();
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"nama penerima like email address is deleted","email address on textfield nama penerima is deleted"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"nama penerima like email address is deleted","email address on textfield nama penerima is not deleted"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkNamaPenerima() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Dian Widya F Sitorus";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"fill nama penerima","text field nama penerima is filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"fill nama penerima","text field nama penerima is not filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkNomorTelepon() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "082364081620";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"fillnomor telepon penerima","text field nomor telepon penerima is filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"fillnomor telepon penerima","text field nomor telepon penerima is not filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkAlamat() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Institut Teknologi Del sitoluama laguboti Tobasa";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"fill alamat penerima","text field alamat penerima is filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"fill alamat penerima","text field alamat penerima is not filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkProvinsi() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Sumatera Utara";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click provinsi penerima","provinsi is filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click provinsi penerima","provinsi is not filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkKota() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Kab. Toba Samosir";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click kota penerima","kota is filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click kota penerima","kota is not filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkKecamatan() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Laguboti";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click kecamatan penerima","kecamatan is filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click kecamatan penerima","kecamatan is not filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkKelurahan() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Sitoluama";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click kelurahan penerima","kelurahan is filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click kelurahan penerima","kelurahan is filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickBtnSimpan() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Alamat Pengiriman";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click button simpan","information about alamat penerima is filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click button simpan","information about alamat penerima is not filled"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClickPilihWarna() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Warna: Hitam";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click color","colors is clicked and show on top of result sepatusport "+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click color","colors is not clicked and not show on top of result sepatusport "+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkClicksepatu() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "39";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click item sepatu sport","sepatu sport is clicked and open description page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click item sepatu sport","sepatu sport is not clicked and not open description page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }


    public void checkThankYouPage() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Terima Kasih telah berbelanja di Blibli.com";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"thankyou page","show thankyou page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"thankyou page","not show thankyou page"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }
    public void checkButtonKategori() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Kategori";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"kategori on homepage","kategori is clicked and choose"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"kategori on homepage","kategori is not clicked and not choose"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }
    public void checkGaleriIndonesia() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Kategori";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"Galeri indonesia in kategori","kategori galeri indonesia is clicked and choose"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"Galeri indonesia in homepage","kategori galeri indonesia is not clicked and not choose"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }

    public void checkBtnBayarSekarang() throws InterruptedException {
        BlibliUtility blibliUtility = new BlibliUtility(webDriver);
        String expectedPage = "Terima Kasih telah berbelanja di Blibli.com";
        if(expectedPage.equals(expectedPage)) {
            test.log(LogStatus.PASS,"click button bayar sekarang","show thankyou page and detail pembayaran"+test.addScreenCapture(blibliUtility.getScreenshot()));
        } else {
            test.log(LogStatus.FAIL,"click button bayar sekarang","not show thankyou page and detail pembayaran"+test.addScreenCapture(blibliUtility.getScreenshot()));
        }
    }


    }

