package com.blibli.future;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BlibligetProperties {
    public String getBlibliProperty(String variabel){
        Properties prop = new Properties();

        try{
            InputStream input = new FileInputStream("BlibliRetail.properties");
            prop.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop.getProperty(variabel);
    }
}
