package com.blibli.future;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class BlibliPembayaran {
    WebDriver webDriver;

    By xpathElementSelectPayment = By.xpath("//li[@class='ng-scope selected-payment']//div[@class='radio-box']");
    By xpathElementOptionPayment = By.xpath("//select[@id='gdn-payment-option-']");
    By xpathElementBayarSekarang = By.xpath("//label[contains(text(),'Bayar Sekarang')]");
    public BlibliPembayaran(WebDriver webDriver) {
        this.webDriver = webDriver;
    }


    public void clickBtnBayarSekarang() throws InterruptedException {
        Thread.sleep(8000);
        WebElement ButtonBayarSekarang = webDriver.findElement(xpathElementBayarSekarang);
        ButtonBayarSekarang.click();
    }

}
