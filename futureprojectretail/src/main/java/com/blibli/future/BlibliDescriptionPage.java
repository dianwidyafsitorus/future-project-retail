package com.blibli.future;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BlibliDescriptionPage {
    WebDriver webDriver;
    By xpathElementBtnBeliSekarang = By.xpath("//div[@class='buy-now__button']//div[@class='button'][contains(text(),'BELI SEKARANG')]");
    By xpathElementBtnTambahKeBag = By.xpath("//div[@class='add-to-cart__button']//div[@class='button'][contains(text(),'TAMBAH KE BAG')]");
    By xpathElementTambahBarang = By.xpath("//div[@class='quantity__increase']");
    By xpathElementKurangBarang = By.xpath("//div[@class='quantity__decrease']");
    By xpathElementclickBag = By.xpath("//div[@id='gdn-cart-button']//a[@class='b-flex b-ai-center']//img");
    By xpathjumlahbarang = By.xpath("//div[@class='quantity__field']//input");
    By xpathElementTambahWishList = By.xpath("//div[@class='wishlist']//img[@class='icon-class']");
    By xpathElementLihatPromo = By.xpath("//div[@class='product__promo__view-all']");
    By xpathElementLihatInfoPromo = By.xpath("//div[@class='modal-mask']//div[1]//div[2]//span[1]");


    public BlibliDescriptionPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void clickBtnBeliSekarang() throws InterruptedException {
        WebElement ButtonBeliSekarang = webDriver.findElement(xpathElementBtnBeliSekarang);
        ButtonBeliSekarang.click();
        Thread.sleep(3000);
    }

    public void clickBtnTambahKeBag() throws InterruptedException {
        WebElement ButtonTambahKeBag = webDriver.findElement(xpathElementBtnTambahKeBag);
        ButtonTambahKeBag.click();
        Thread.sleep(3000);
    }

    public void clickBag() throws InterruptedException {
        WebElement iconBag = webDriver.findElement(xpathElementclickBag);
        iconBag.click();
        Thread.sleep(3000);
    }

    public void clickTambahKuantitas() throws InterruptedException {
        Thread.sleep(3000);
        WebElement TambahKuantitas = webDriver.findElement(xpathElementTambahBarang);
        TambahKuantitas.click();
    }

    public void clickKurangKuantitas() throws InterruptedException {
        Thread.sleep(3000);
        WebElement KurangKuantitas = webDriver.findElement(xpathElementKurangBarang);
        KurangKuantitas.click();
    }

    public void jumlahbarang0() throws InterruptedException {
        WebElement barang0 = webDriver.findElement(xpathjumlahbarang);
        barang0.clear();
        Thread.sleep(6000);
    }
    public void TambahWishlist() throws InterruptedException {
        WebElement wishlist = webDriver.findElement(xpathElementTambahWishList);
        wishlist.click();
        Thread.sleep(3000);
    }
    public void LihatPromo() throws InterruptedException {
        WebElement lihatpromo = webDriver.findElement(xpathElementLihatPromo);
        lihatpromo.click();
        Thread.sleep(3000);
    }
    public void LihatInfoPromo() throws InterruptedException {
        WebElement lihatInfopromo = webDriver.findElement(xpathElementLihatInfoPromo);
        lihatInfopromo.click();
        Thread.sleep(3000);
    }

}
