package com.blibli.future;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class BlibliSearchPage {
    WebDriver webDriver;
    By xpathElementBoxSearch = By.xpath("//input[@placeholder='Kamu lagi cari apa?']");
    By xpathElementItemgulaku = By.xpath("//body/div[@class='background-grey']/div[@class='collabs-wrapper']/div/div/div[@class='container']/div[@id='blibli-main-ctrl']/section[@class='content-section']/div[@id='catalogProductListDiv']/div[@class='product-listing']/div[@id='productListingAgeRestrictedFilter']/div[@class='large-16 medium-16 small-16']/div[@id='catalogProductListContentDiv']/div[@class='product columns']/div[1]/div[1]/div[1]/a[1]/div[1]/div[1]/img[1]");
    By xpathElementwarna = By.xpath("//div[contains(text(),'hitam')]");
   By xpathElementMetodePengiriman = By.xpath("//div[@class='multi-select-filter logisticOption-filter']//li[1]//label[1]//div[1]");
    By xpathElementkliksepatu2 = By.xpath("//body/div[@class='background-grey']/div[@class='collabs-wrapper']/div/div/div[@class='container']/div[@id='blibli-main-ctrl']/section[@class='content-section']/div[@id='catalogProductListDiv']/div[@class='product-listing']/div[@id='productListingAgeRestrictedFilter']/div[@class='large-16 medium-16 small-16']/div[@id='catalogProductListContentDiv']/div[@class='product columns']/div[1]/div[1]/div[1]/a[1]/div[1]/div[1]/img[1]");
    By xpathElementklikBarang = By.xpath("//body/div[@class='background-grey']/div[@class='collabs-wrapper']/div/div/div[@class='container']/div[@id='blibli-main-ctrl']/section[@class='content-section']/div[@id='catalogProductListDiv']/div[@class='product-listing']/div[@id='productListingAgeRestrictedFilter']/div[@class='large-16 medium-16 small-16']/div[@id='catalogProductListContentDiv']/div[@class='product columns']/div[1]/div[1]/div[1]/a[1]/div[1]/div[1]/img[1]");


    public BlibliSearchPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void clearTextfieldSearch() throws InterruptedException {
       WebElement BoxSearch = webDriver.findElement(xpathElementBoxSearch);
       BoxSearch.clear();
       Thread.sleep(8000);
    }

    public void clickItemgulaku() throws InterruptedException {
        Thread.sleep(8000);
        WebElement Itemgulaku = webDriver.findElement(xpathElementItemgulaku);
        Itemgulaku.click();

    }

    public void checklistwarna() throws InterruptedException {
        Thread.sleep(10000);
        WebElement clickwarna = webDriver.findElement(xpathElementwarna);
        if (clickwarna != null) {
            clickwarna.click();
        }
    }

    public void clickItemSepatu2(){
        WebElement kliksepatu2 = webDriver.findElement(xpathElementkliksepatu2);
        kliksepatu2.click();
    }

    public void clickItemTas(){
        WebElement klikTas = webDriver.findElement(xpathElementklikBarang);
        klikTas.click();
    }

    public void clickmetodePengiriman() throws InterruptedException {
        WebElement metodepembayaran = webDriver.findElement(xpathElementMetodePengiriman);
        metodepembayaran.click();
        Thread.sleep(6000);
    }




}
