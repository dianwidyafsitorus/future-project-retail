package com.blibli.future;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Random;


public class BlibliHomePage{

    WebDriver webDriver;

    By xpathElemetBtnDaftar = By.xpath("//button[@class='blu-btn btn__register b-small']");
    By xpathElementinputRegister = By.className("form__input");
    By xpathElementinputpasswordregister = By.className("form__input");
    By xpathElementCheckboxsetuju = By.xpath("//div[@class='checkbox container-agreement']//label");
    By xpathElementBtnSubmitDaftar = By.xpath("//button[@class='blu-btn b-full-width b-warning']");
    By xpathElementinputEmail = By.xpath("//input[@placeholder='Masukkan email']");
    By xpathElementinputKatasandi = By.xpath("//input[@placeholder='Masukkan kata sandi']");
    By xpathElementBtnSubmit = By.xpath("//button[@class='blu-btn b-full-width b-warning']");
    By xpathElementHeaderName = By.xpath("//div[@class='account__text']");
    By xpathElementHeaderName2 = By.xpath("/html/body/div[5]/div/header/div[3]/section/div/div/div/ul/li[2]/div/div[2]/div/div[2]/div/div[2]/div[4]/label[2]/img");
    By xpathElementBtnLogOut = By.xpath("/html/body/div[5]/div/header/div[3]/section/div/div/div/ul/li[2]/div/div[2]/div/div[2]/div/div[2]/div[4]/div/div/div[2]/span[2]/a/img");
    By xpathElementBtnMasuk = By.xpath("//button[@class='blu-btn btn__login b-outline b-small']");
    By xpathElementBoxSearch = By.xpath("//input[@placeholder='Kamu lagi cari apa?']");
    By xpathElementBtnCari = By.xpath("//button[@class='blu-btn b-secondary b-small']");
    By xpathElementClickLewati = By.className("blu-ripple");
    By xpathElementbeforelogout = By.xpath("//div[@class='wrapper-menu b-overflow-y']//a[1]");
    By xpathKategori = By.xpath("//div[@class='kategori__trigger b-flex b-ai-center']//span[contains(text(),'Kategori')]");
    By xpathGaleriIndonesia = By.xpath("//a[contains(text(),'Galeri Indonesia')]");
    By xpathElementWishlist = By.xpath("//header[@class='header']//a[5]");
    By xpathLogoUser  = By.xpath("//div[@class='account tooltip__trigger']//div[@class='b-flex b-ai-center']");
    By xpathLogout = By.xpath("//span[contains(text(),'Keluar')]");

    public BlibliHomePage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }


    public void openBlibli(WebDriver webDriver){
        webDriver.get("https://www.blibli.com");
    }

    public void clickLewati() throws InterruptedException {
        Thread.sleep(3000);
        try{
           WebElement Close = webDriver.findElement(xpathElementClickLewati);
            Close.click();
       }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void closeIklan() throws InterruptedException {
        Thread.sleep(3000);
        try {
           new Actions(webDriver).sendKeys(Keys.ESCAPE).perform();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }



    public void clickBtnDaftar() throws InterruptedException {
        Thread.sleep(4000);
       WebElement ButtonDaftar = webDriver.findElement(xpathElemetBtnDaftar);
       ButtonDaftar.click();
    }

    public void inputRegister(String usernameRegister, String passwordRegister) throws InterruptedException {
        WebElement inputEmailRegister = webDriver.findElement(xpathElementinputRegister);
        inputEmailRegister.sendKeys(usernameRegister);
        WebElement inputPasswordRegister = webDriver.findElement(xpathElementinputpasswordregister);
        inputPasswordRegister.sendKeys(passwordRegister);

    }

    public void clickBtnSubmitDaftar() throws InterruptedException {
        WebElement checklist = webDriver.findElement(xpathElementCheckboxsetuju);
        checklist.click();
        WebElement ButtonSubmitDaftar = webDriver.findElement(xpathElementBtnSubmitDaftar);
        ButtonSubmitDaftar.click();
        Thread.sleep(4000);
    }

    public void clickBtnSubmit() throws InterruptedException {
        WebElement ButtonSubmit = webDriver.findElement(xpathElementBtnSubmit);
        ButtonSubmit.click();
        Thread.sleep(6000);
    }

    public void beforelogout() throws InterruptedException {
        WebElement header = webDriver.findElement(xpathElementHeaderName);
        header.click();
       Thread.sleep(6000);
       WebElement beforeLogout = webDriver.findElement(xpathElementbeforelogout);
       beforeLogout.click();
    }

    public void clickWishlist() throws InterruptedException {
        WebElement HeaderName2 = webDriver.findElement(xpathElementHeaderName2);
        HeaderName2.click();
        Thread.sleep(6000);
        WebElement wishlist = webDriver.findElement(xpathElementWishlist);
        wishlist.click();
    }

    public void logout() throws InterruptedException {
        WebElement HeaderName2 = webDriver.findElement(xpathElementHeaderName2);
        HeaderName2.click();
        Thread.sleep(6000);
        WebElement ButtonLogOut = webDriver.findElement(xpathElementBtnLogOut);
        ButtonLogOut.click();
    }


    public void clickBtnMasuk() throws InterruptedException {
        WebElement ButtonMasuk = webDriver.findElement(xpathElementBtnMasuk);
        ButtonMasuk.click();
        Thread.sleep(6000);
    }

    public void inputMasuk(String username, String password) throws InterruptedException {
        WebElement inputEmail = webDriver.findElement(xpathElementinputEmail);
        inputEmail.sendKeys(username);
        WebElement inputPassword = webDriver.findElement(xpathElementinputKatasandi);
        inputPassword.sendKeys(password);

    }

    public void inputSearch(String barang) throws InterruptedException {
        WebElement BoxSearch = webDriver.findElement(xpathElementBoxSearch);
        BoxSearch.sendKeys(barang);
        WebElement ButtonCari = webDriver.findElement(xpathElementBtnCari);
        ButtonCari.click();
        Thread.sleep(3000);
    }

    public void clickbtnKategori() throws InterruptedException {
        Thread.sleep(4000);
        WebElement ButtonKategori = webDriver.findElement(xpathKategori);
        ButtonKategori.click();
    }

    public void clickGaleriIndonesia() throws InterruptedException {
        Thread.sleep(4000);
        WebElement galeriIndonesia = webDriver.findElement(xpathGaleriIndonesia);
        galeriIndonesia.click();
    }

    public static String createRandomUsername(){
        final String alphabet = "0123456789ABCDEfghijklmnopqrstuvwxyz";
        final int N = alphabet.length();
        String result = "";
        Random r = new Random();
        for (int i = 0; i < 10; i++) {
            result+= alphabet.charAt(r.nextInt(N));
        }
        return result;
    }




}
