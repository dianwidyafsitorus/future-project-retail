package com.blibli.future;

import javafx.stage.Screen;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.io.IOException;

public class BlibliUtility {
    WebDriver webDriver;

    public BlibliUtility(WebDriver webDriver) {
        this.webDriver = webDriver;
    }
    public String getScreenshot() throws InterruptedException {
        Thread.sleep(3000);
        TakesScreenshot scrShot =((TakesScreenshot)webDriver);
        File src=scrShot.getScreenshotAs(OutputType.FILE);
        String path = System.getProperty("user.dir")  +"/src/BlibliReport/screenshoot/"+ System.currentTimeMillis() +".png";
        File destination = new File(path);
        try {
            FileUtils.copyFile(src,destination);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return path;
    }



}
