package com.blibli.future;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BlibliBagPage {
    WebDriver webDriver;
    By xpathElementBtnCheckOut = By.xpath("//button[@class='blu-btn b-primary']");
    By xpathElementHapusBarang = By.xpath("//span[contains(text(),'Hapus')]");
    By xpathElementBelanjaLagi = By.xpath("//span[@class='cart__title__back-shopping__text']");

    public BlibliBagPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }


    public void clickBtnCheckOut() throws InterruptedException {
        WebElement ButtonCheckOut = webDriver.findElement(xpathElementBtnCheckOut);
        ButtonCheckOut.click();
        Thread.sleep(4000);
    }

    public void clickHapusBarang() throws InterruptedException {
        WebElement clickHapus = webDriver.findElement(xpathElementHapusBarang);
        clickHapus.click();
        Thread.sleep(4000);
    }

    public void clickbelanjaLagi() throws InterruptedException {
        WebElement BelanjaLagi = webDriver.findElement(xpathElementBelanjaLagi);
        BelanjaLagi.click();
        Thread.sleep(4000);
    }


}
