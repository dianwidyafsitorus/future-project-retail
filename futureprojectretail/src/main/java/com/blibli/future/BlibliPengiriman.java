package com.blibli.future;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class BlibliPengiriman {
    WebDriver webDriver;
    By xpathElementinputAlamat = By.xpath("//textarea[@id='gdn-adrStreet']");
    By xpathElementBtnLanjutkan = By.xpath("//button[@class='blu-btn b-jc-center next-btn b-primary']");
    By xpathElementTambahAlamat = By.xpath("//div[@class='blu-column']");
    By xpathElementinputNamapenerima = By.xpath("//input[@id='gdn-receiptNm']");
    By xpathElementinputNoTelepon = By.xpath("//input[@id='phone']");
    By xpathElementBtnSimpan = By.xpath("//button[@class='blu-btn b-secondary save-btn']");
    By xpathElementpilihProvinsi = By.xpath("//select[@id='province']");
    By xpathElementpilihKota = By.xpath("//div[@class='blu-column']//div[3]//select[1]");
    By xpathElementpilihKecamatan = By.xpath("//div[4]//select[1]");
    By xpathEleementpilihKelurahan = By.xpath("//div[5]//select[1]");


    public BlibliPengiriman(WebDriver webDriver) {
        this.webDriver = webDriver;
    }


    public void clickBtnLanjut() throws InterruptedException {
        Thread.sleep(6000);
        WebElement ButtonLanjut = webDriver.findElement(xpathElementBtnLanjutkan);
        ButtonLanjut.click();
    }

    public void clickTambahAlamat(){
        WebElement TambahAlamat = webDriver.findElement(xpathElementTambahAlamat);
        TambahAlamat.click();
    }

    public void inputNoTelepon( String NoTelepon){
        WebElement inputNoTelepon = webDriver.findElement(xpathElementinputNoTelepon);
        inputNoTelepon.sendKeys(NoTelepon);
    }

    public void inputAlamatPengiriman(String alamat) throws InterruptedException {
        WebElement inputAlamat = webDriver.findElement(xpathElementinputAlamat);
        inputAlamat.sendKeys(alamat);
    }
    public void inputNamapenerima(String nama){
        WebElement inputnama = webDriver.findElement(xpathElementinputNamapenerima);
        inputnama.sendKeys(nama);
    }

    public void clearTextfieldNamaPengirim() throws InterruptedException {
        WebElement BoxSearch = webDriver.findElement(xpathElementinputNamapenerima);
        BoxSearch.clear();
        Thread.sleep(8000);
    }

    public void clickpilihProvinsi(){
        WebElement pilihProvinsi = webDriver.findElement(xpathElementpilihProvinsi);
        Select provinsi = new Select(pilihProvinsi);
        provinsi.selectByVisibleText("Sumatera Utara");
    }

    public void clickpilihKota(){
        WebElement pilihkota = webDriver.findElement(xpathElementpilihKota);
        Select kota = new Select(pilihkota);
        kota.selectByVisibleText("Kab. Toba Samosir");
    }

    public void clickpilihKecamatan(){
        WebElement pilihkecamatan = webDriver.findElement(xpathElementpilihKecamatan);
        Select kecamatan = new Select(pilihkecamatan);
        kecamatan.selectByVisibleText("Laguboti");
    }

    public void clickpilihKelurahan(){
        WebElement pilihkelurahan = webDriver.findElement(xpathEleementpilihKelurahan);
        Select kelurahan = new Select(pilihkelurahan);
        kelurahan.selectByVisibleText("Sitoluama");
    }

    public void clickBtnSimpan(){
        WebElement ButtonSimpan = webDriver.findElement(xpathElementBtnSimpan);
        ButtonSimpan.click();
    }







}
